import avatar from './assets/images/48.jpg'
import './App.css'

function App() {
  return (
    <div className='devcamp'>
      <div className='devcamp-wrapper'><img alt='avatar' className='devcamp-avatar' src={avatar} />
        <div className='devcamp-quote'>
          <p>This is one of the best developer blogs on the plannet. I read it daily to improve my skills </p>
        </div>
        <p className='devcamp-name'>Tammy Stevens.<span>- Front end developer</span></p></div>
    </div>

  );
}

export default App;
